package reverse

import "github.com/isdamir/gotype"

func InsertReverse(node *gotype.LNode) {
	if node == nil || node.Next == nil {
		return
	}
	var cur, next *gotype.LNode
	cur = node.Next.Next
	//设置链表第一个结点为尾结点
	node.Next.Next = nil
	//把遍历到的结点插入到头结点的后面
	for cur != nil {
		next = cur.Next
		cur.Next = node.Next
		node.Next = cur
		cur = next
	}
}
