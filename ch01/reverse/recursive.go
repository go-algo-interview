package reverse

import "github.com/isdamir/gotype"

func RecursiveReverseChild(node *gotype.LNode) *gotype.LNode {
	if node == nil || node.Next == nil {
		return node
	}
	newHead := RecursiveReverseChild(node.Next)
	node.Next.Next = node
	node.Next = nil
	return newHead
}
func RecursiveReverse(node *gotype.LNode) {
	firstNode := node.Next
	newHead := RecursiveReverseChild(firstNode)
	node.Next = newHead
}
